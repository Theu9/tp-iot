# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "iot_database" {
  database_name = "iot"
}

# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "Tempe_Tab_Sensor" {
  database_name = aws_timestreamwrite_database.iot_database.database_name
  table_name    = "temperaturesensor"
}
