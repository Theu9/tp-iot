# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
}
# aws_iot_policy pub-sub
resource "aws_iot_policy" "pubsub" {
  name = "UniquePolicyName"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iot:*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}
# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}
# aws_iot_thing temp_sensor
resource "aws_iot_thing" "example" {
  name = "example"

  attributes = {
    First = "examplevalue"
  }
}
# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.example.name
}
# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
# Add this data source block to your Terraform configuration
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}


# aws_iot_topic_rule rule for sending invalid data to DynamoDB

resource "aws_iot_topic_rule" "dynamodb_rule" {
  name        = "MyRule"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+' WHERE temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.TableauTemperature.name
    }
  }
}
 #aws_iam_role_policy
resource "aws_iam_role_policy" "dynamodb_policy" {
  name   = "DynamoDBPolicy"
  role   = aws_iam_role.iot_role.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "dynamodb:PutItem",
      "Resource": "*"
    }
  ]
}
EOF
}

# aws_iot_topic_rule rule for sending valid data to Timestream
# iot.tf

# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "timestream_rule" {
  name        = "TimestreamRule"
  description = "Rule for sending valid data to Timestream"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    role_arn      = aws_iam_role.iot_role.arn
    database_name = "iot"
    table_name    = "temperaturesensor"# Fix this line

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}



###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
